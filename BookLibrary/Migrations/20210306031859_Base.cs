﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookLibrary.Migrations
{
    public partial class Base : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Emp_id = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Emp_name = table.Column<string>(nullable: true),
                    Card_no = table.Column<string>(nullable: true),
                    Pos_id = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    Factory = table.Column<string>(nullable: true),
                    Section = table.Column<string>(nullable: true),
                    Dept = table.Column<string>(nullable: true),
                    Workshop = table.Column<string>(nullable: true),
                    Line_no = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Emp_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee");
        }
    }
}
