﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookLibrary.Migrations
{
    public partial class AddEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Employee",
                columns: new[] { "Emp_id", "Card_no", "Dept", "Emp_name", "Factory", "Line_no", "Password", "Pos_id", "Section", "Sex", "Status", "Workshop" },
                values: new object[] { "A274080", "123123", "MIS", "Laaa", "VNC", "MIS", "123456", "M1000", "MIS", "Đực", true, "MIS" });

            migrationBuilder.InsertData(
                table: "Employee",
                columns: new[] { "Emp_id", "Card_no", "Dept", "Emp_name", "Factory", "Line_no", "Password", "Pos_id", "Section", "Sex", "Status", "Workshop" },
                values: new object[] { "A274082", "123123", "MIS", "Lâcscascascaaa", "VNC", "MIS", "123456", "M1000", "MIS", "Đực", true, "MIS" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumn: "Emp_id",
                keyValue: "A274080");

            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumn: "Emp_id",
                keyValue: "A274082");
        }
    }
}
