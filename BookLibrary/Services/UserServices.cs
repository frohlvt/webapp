﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace WebApplication2.BookLibrary
{
    public interface IUserServices
    {
        Employee GetEmployee(string username, string password, bool isAdmin = false);

        Employee AddEmployee(string jsonValues);

        Employee UpdateEmployee(string key, string jsonValues);

        Employee RemoveEmployee(string key);

        List<Employee> GetEmployees();
    }

    public class UserServices : IUserServices
    {
        private readonly UserContext userContext;
        public UserServices(UserContext userContext)
        {
            this.userContext = userContext;
        }

        public Employee AddEmployee(string jsonValues)
        {
            var employee = new Employee();
            JsonConvert.PopulateObject(jsonValues, employee);
            var existed = userContext.Employees.FirstOrDefault(x => x.EmpId == employee.EmpId);
            if(existed == null)
            {
                userContext.Employees.Add(employee);
                userContext.SaveChanges();
            }

            return employee;
        }

        public Employee GetEmployee(string username, string password, bool isAdmin = false)
        {
            return userContext.Employees.Where(x => x.EmpId.ToUpper() == username.ToUpper()
                                                            && (isAdmin || x.Password == password)).FirstOrDefault();
        }

        public List<Employee> GetEmployees()
        {
            return userContext.Employees.ToList();
        }

        public Employee RemoveEmployee(string key)
        {
            var emp = userContext.Employees.FirstOrDefault(x => x.EmpId == key);
            if(emp != null)
            {
                userContext.Remove(emp);
                userContext.SaveChanges();
            }
            return emp;
        }

        public Employee UpdateEmployee(string key, string jsonValues)
        {
            var emp = userContext.Employees.FirstOrDefault(x => x.EmpId == key);
            if (emp != null)
            {
                JsonConvert.PopulateObject(jsonValues, emp);
                userContext.SaveChanges();
            }
            return emp;
        }
    }
}
