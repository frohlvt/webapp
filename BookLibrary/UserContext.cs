﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication2.BookLibrary
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasKey(c => c.EmpId);
            var employees = new List<Employee>();
            employees.Add(new Employee()
            {
                EmpId = "A274080",
                Card_no = "123123",
                Dept= "MIS",
                EmpName = "Laaa",
                Factory = "VNC",
                LineNo = "MIS",
                Password = "123456",
                Pos_id = "M1000",
                Section = "MIS",
                Sex = "Đực",
                Status = true,
                Workshop = "MIS"
            });            
            employees.Add(new Employee()
            {
                EmpId = "A274082",
                Card_no = "123123",
                Dept= "MIS",
                EmpName = "Lâcscascascaaa",
                Factory = "VNC",
                LineNo = "MIS",
                Password = "123456",
                Pos_id = "M1000",
                Section = "MIS",
                Sex = "Đực",
                Status = true,
                Workshop = "MIS"
            });

            modelBuilder.Entity<Employee>().HasData(employees);
        }
    }

    [Table("Employee")]
    public class Employee
    {
        [Key]
        [Column("Emp_id")]
        public string EmpId { get; set; }
        public string Password { get; set; }
        [Column("Emp_name")]
        public string EmpName { get; set; }
        public string Card_no { get; set; }
        public string Pos_id { get; set; }
        public string Sex { get; set; }
        public string Factory { get; set; }
        public string Section { get; set; }
        public string Dept { get; set; }
        public string Workshop { get; set; }
        [Column("Line_no")]
        public string LineNo { get; set; }
        public bool? Status { get; set; }
    }

}
