﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.BookLibrary;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserServices userServices;

        public HomeController(ILogger<HomeController> logger,IUserServices userServices)
        {
            _logger = logger;
            this.userServices = userServices;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Users
        public IActionResult Employee()
        {
            return View();
        }
        public ActionResult GetEmployees(DataSourceLoadOptions options)
        {
            var data = userServices.GetEmployees();
            return Json(DataSourceLoader.Load(data, options));
        }

        public ActionResult AddEmployee(string values)
        {
            var data = userServices.AddEmployee(values);
            return Json(new { result = true, data });
        }

        public ActionResult EditEmployee(string key, string values)
        {
            var data = userServices.UpdateEmployee(key, values);
            return Json(new { result = true, data });
        }

        public IActionResult RemoveEmployee(string key)
        {
            var data = userServices.RemoveEmployee(key);
            return Json(new { result = true, data });
        }
        #endregion
    }
}
